# STUDENT MANAGEMENT SERVICE

## LAUNCH APPLICATION

    ./gradlew bR

## Database initial data
SELECT * FROM STUDENT;

| ID | BIRTH_DAY  | COMMENT | FIRST_NAME | KEY_NUMBER |
| -- | ---------- | ------- | ---------- | ---------- |
| 1  | 1996-10-02 | comme.. | Vasiliy    |  1D704612  |

| LAST_NAME | PATRONYMIC |GROUP_ID |
| --------- | ---------- | ------- |
|  Balayev  | Egorovich  |    1    |

SELECT * FROM STUDY_GROUP;

| ID | COMMENT | NAME |
| -- | ------- | ---- |
| 1  |  null   | it45 |
| 2  |for pat..|cs42 |

## REST API

### Get all students
GET: http://localhost:8080/students/ - to see all students

### Add new student
POST: http://localhost:8080/students/ with body:
```json
{
	"firstName": "John",
	"lastName": "Woo",
	"birthDay": "1988-02-15",
	"group": {
            "id": 1,
            "name": "it45",
            "comment": null
        }
}
```
### Replace student
PUT: http://localhost:8080/students/{id} with json body (like in 'post' method) to replace student
### Update student data
PATCH: http://localhost:8080/students/{id} - to update necessary fields\*
```json
{
	"firstName": "Jack",
	"birthDay": "1999-09-09"
}
```
\*for study group changing use "groupId" field and number value:
```json
{
	...
	"groupId": 3,
	...
}
```
### Remove student
DELETE: http://localhost:8080/students/{id}
