package sboot.students.backend;

import java.time.LocalDate;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import sboot.students.backend.model.Student;
import sboot.students.backend.model.StudentsUtils;
import sboot.students.backend.model.StudyGroup;
import sboot.students.backend.repos.StudentRepository;
import sboot.students.backend.repos.StudyGroupRepository;

@Component
public class ApplicationRunner implements CommandLineRunner {
	
	private static final Logger log = LoggerFactory.getLogger(ApplicationRunner.class);

	@Autowired
	StudentRepository studentRepository;
	@Autowired
	StudyGroupRepository groupRepository;

	@Override
	public void run(String... args) throws Exception {
		log.info("INSIDE RUNNER");

		StudyGroup group = new StudyGroup("it45");
		try {
			group = groupRepository.save(group);
		} catch (Exception ex) {
			log.error("EXCEPTION when groupRepository.save(group)", ex);
		}

		Student vasya = new Student("Vasiliy", "Balayev");
		vasya.setBirthDay(LocalDate.of(1996, 10, 2));
//		vasya.setPatronymic("Petrovich");  - default patronymic "Egorovich" will be set
		vasya.setKeyNumber(StudentsUtils.getUuidShortKey());
		vasya.setGroup(group);

		try {
			studentRepository.save(vasya);
		} catch (Exception ex) {
			log.error("EXCEPTION when studentRepository.save(vasya)", ex);
		}

		StudyGroup anotherGroup = new StudyGroup("cs42");
		anotherGroup.setComment("for patch testing");
		try {
			anotherGroup = groupRepository.save(anotherGroup);
		} catch (Exception ex) {
			log.error("EXCEPTION when groupRepository.save(group)", ex);
		}
	}
}
