package sboot.students.backend.web;

import java.util.List;
import java.util.Map;

import static sboot.students.backend.AppUtils.*;
import sboot.students.backend.model.StudentsUtils;
import sboot.students.backend.repos.StudentRepository;
import sboot.students.backend.repos.StudyGroupRepository;
import sboot.students.backend.web.exceptions.StudentNotFoundException;
import sboot.students.backend.model.StudentFieldsRegister;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import sboot.students.backend.model.Student;
import sboot.students.backend.web.exceptions.StudentWrongDataException;

@RestController
class StudentController {

    private final StudentRepository repository;
    private final StudyGroupRepository groupRepository;

    StudentController(StudentRepository repository, StudyGroupRepository groupRepository) {
        this.repository = repository;
        this.groupRepository = groupRepository;
    }

    @RequestMapping("/")
    public String index() {
        return "Wellcome to the students REST Api (use /students endpoint to get records)";
    }

    @GetMapping("/students")
    List<Student> getStudents() { return (List<Student>) repository.findAll(); }

    @PostMapping("/students")
    @ResponseStatus(HttpStatus.CREATED)
    Student createStudent(@RequestBody Student newStudent) {
        newStudent.setKeyNumber(StudentsUtils.getUuidShortKey());
        try {
            return repository.save(newStudent);
        } catch (DataIntegrityViolationException dataEx) {
            throw new StudentWrongDataException(dataEx.getMessage());
        }
    }

    @GetMapping("/students/{id}")
    Student getStudent(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(() -> new StudentNotFoundException(id));
    }

    @PutMapping("/students/{id}")
    Student replaceStudent(@RequestBody Student newStudent, @PathVariable Long id) {
        newStudent.setId(id);
        Student oldStudent = repository.findById(id).orElseThrow(() -> new StudentNotFoundException(id));
        newStudent.setKeyNumber(oldStudent.getKeyNumber());
        return repository.save(newStudent);
    }

    @PatchMapping("/students/{id}")
    Student updateStudentFields(@RequestBody Map<String, Object> studentData, @PathVariable Long id) {

        Student student = repository.findById(id).orElseThrow(() -> new StudentNotFoundException(id));
        for (Map.Entry<String, Object> change : studentData.entrySet()) {
            if(change.getKey().equals("keyNumber")) continue;
            Object changedValue;
            if(change.getKey().equals("groupId")){
                long groupId = -1;
                if(change.getValue() instanceof Long){
                    groupId = Long.valueOf((Long) change.getValue());
                    println("************** Group Id from long is: "+groupId);
                }else if (change.getValue() instanceof Integer){
                    groupId = Long.valueOf((Integer) change.getValue());
                    println("************** Group Id from int is: " + groupId);
                }
                changedValue = groupRepository.findById(groupId).orElse(null);
            } else
            changedValue = change.getValue();
            StudentFieldsRegister.valueOf(change.getKey()).changeStudent(student, changedValue);
        }
        return repository.save(student);
    }

    @DeleteMapping("/students/{id}")
    void deleteStudent(@PathVariable Long id) { repository.deleteById(id); }
}
