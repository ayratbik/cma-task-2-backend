package sboot.students.backend.web.exceptions;

public class StudentNotFoundException extends RuntimeException {

	public StudentNotFoundException(Long id) {
		super("Could not find student for id: " + id);
	}
}
