package sboot.students.backend.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class StudentWrongDataAdvice {
	@ResponseBody
	@ExceptionHandler(StudentWrongDataException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	String studentWrongDataHandler(StudentWrongDataException ex) {
		return ex.getMessage();
	}
}
