package sboot.students.backend.web.exceptions;

public class StudentWrongDataException extends RuntimeException {

	public StudentWrongDataException(String msg) {
		super(msg);
	}
}
