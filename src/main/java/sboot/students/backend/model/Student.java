package sboot.students.backend.model;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.Nulls;

import java.time.LocalDate;

import javax.persistence.*;

@Entity
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column
    @JsonSetter(nulls = Nulls.SKIP)
    private String patronymic = "Egorovich";//default value for both: jpa and json
    @Column(nullable = false)
    private LocalDate birthDay;
    @ManyToOne//(cascade = CascadeType.ALL, fetch = FetchType.LAZY) //- it needs to set a bidirectional link
    @JoinColumn(name="group_id", nullable = false)
    private StudyGroup group;
    @Column(nullable = false, unique = true)
    private String keyNumber;
    @Column
    @JsonSetter(nulls = Nulls.AS_EMPTY)// null comment will be transform to empty string
    private String comment = "default comment from jpa";

    protected Student() {
        this.comment = "default comment from json";
    }

    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate lBirthDay) {
        this.birthDay = lBirthDay;
    }

    public String getKeyNumber() {
        return keyNumber;
    }

    public void setKeyNumber(String keyNumber) {
        this.keyNumber = keyNumber;
    }

    public StudyGroup getGroup() { return group; }

    public void setGroup(StudyGroup group) { this.group = group; }

    public String getComment() { return comment; }

    public void setComment(String comment) { this.comment = comment; }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", birthDay=" + birthDay +
                ", group=" + group +
                ", keyNumber='" + keyNumber + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
