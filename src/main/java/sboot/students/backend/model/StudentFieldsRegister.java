package sboot.students.backend.model;

import java.time.LocalDate;
import java.util.function.BiConsumer;
import java.util.function.Function;

public enum StudentFieldsRegister {

    lastName(Student::getLastName, (student, val) -> student.setLastName((String) val)),
    firstName(Student::getFirstName, (student, val) -> student.setFirstName((String) val)),
    birthDay(Student::getBirthDay, (student, val) -> student.setBirthDay(LocalDate.parse((String)val))),
    groupId(student -> student.getGroup().getId(), (student, val) -> student.setGroup((StudyGroup) val)),
    keyNumber(Student::getKeyNumber, (student, val) -> student.setKeyNumber((String) val)),
    comment(Student::getComment, (student, val) -> student.setComment((String) val));

    Function<Student, Object> studentGetter;
    BiConsumer<Student, Object> studentSetter;

    StudentFieldsRegister(
            Function<Student, Object> studentGetter,
            BiConsumer<Student, Object> studentSetter) {
        this.studentGetter = studentGetter;
        this.studentSetter = studentSetter;
    }

    public void changeStudent(Student student, Object value) {
        studentSetter.accept(student, value);
    }
}
