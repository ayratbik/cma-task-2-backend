package sboot.students.backend.model;

import java.util.UUID;

public class StudentsUtils {

    public static String getUuidShortKey() {
        String uuid = UUID.randomUUID().toString().toUpperCase();
        return uuid.substring(0, uuid.indexOf("-"));
    }
}
