package sboot.students.backend.repos;

import org.springframework.data.repository.CrudRepository;
import sboot.students.backend.model.Student;


public interface StudentRepository  extends CrudRepository<Student, Long>{

}
