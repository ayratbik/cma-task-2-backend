package sboot.students.backend.repos;

import org.springframework.data.repository.CrudRepository;
import sboot.students.backend.model.StudyGroup;

public interface StudyGroupRepository extends CrudRepository<StudyGroup, Long> {
}
