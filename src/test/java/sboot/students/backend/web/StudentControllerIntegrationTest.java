package sboot.students.backend.web;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import sboot.students.backend.TestDataProvider;
import sboot.students.backend.model.Student;
import sboot.students.backend.model.StudyGroup;

import java.net.URL;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static sboot.students.backend.AppUtils.println;
import static sboot.students.backend.TestDataProvider.createStudentWithTestData;

@SuppressWarnings("unused")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StudentControllerIntegrationTest {


	@LocalServerPort
	private int port;

	private URL baseUrl;
	private String studentsUrl;

	@Autowired
	private TestRestTemplate template;
	@Autowired
	private TestDataProvider dataProvider;

	private StudyGroup group;

	public StudentControllerIntegrationTest() {
	}

	@BeforeEach
	public void setUp() throws Exception {
		
		this.baseUrl = new URL("http://localhost:" + port);
		this.studentsUrl = baseUrl + "/students";

		Student[] students = template.getForEntity(studentsUrl, Student[].class).getBody();
		println("******** Found "+students.length+" student(s)");
		for(Student s: students){
			template.delete(studentsUrl +"/" + s.getId());
		}

		group = dataProvider.getTestStudyGroup();
	}
	
	@Test
	public void testIndex() {
		ResponseEntity<String> response = template.getForEntity(baseUrl.toString(), String.class);
		assertThat(response.getBody()).contains("Wellcome to the students REST Api");
	}

	@Test
	public void testCreateStudent(){
		Student student = createStudentWithTestData(group);
		ResponseEntity<Student> response = template.postForEntity(studentsUrl, student, Student.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
	}

	@Test
	public void testReplaceStudent() {
		Student student = createStudentWithTestData(group);
		ResponseEntity<Student> response = template.postForEntity(studentsUrl, student, Student.class);
		student = response.getBody();
		String firstName = "testFirstName";
		String lastName = "testLastName";
		Student student2 = new Student(firstName, lastName);
		student2.setGroup(student.getGroup());
		student2.setBirthDay(LocalDate.parse("1999-01-31"));
		template.put(studentsUrl + "/" + student.getId(), student2);
		response = template.getForEntity(studentsUrl + "/" + student.getId(), Student.class);
		student = response.getBody();
		assertThat(student.getFirstName()).isEqualTo(firstName);
		//...
	}

	@Test
	public void testUpdateStudentFields() {
		Student student = createStudentWithTestData(group);
		ResponseEntity<Student> response = template.postForEntity(studentsUrl, student, Student.class);
		student = response.getBody();
		println("******** Before: " + student.toString());

		String firstName = "testFirstName";
		String lastName = "testLastName";
		String birthDay = "1999-01-31";
		StudyGroup anotherGroup = dataProvider.getAnotherTestStudyGroup();
		Long groupId = anotherGroup.getId();
		Map<String, Object> studentData = new HashMap<>();
		studentData.put("firstName", firstName);
		studentData.put("lastName", lastName);
		studentData.put("groupId", groupId);
		studentData.put("birthDay",birthDay);

		template.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory());

		student=template.patchForObject(studentsUrl + "/" + student.getId(), studentData, Student.class);

		//Another way to execute http patch request:
		// ==================================
//		HttpEntity<Map<String, String>> httpEntity = new HttpEntity<>(studentData);
//		response = template.exchange(studentsUrl + "/" + student.getId(), HttpMethod.PATCH, httpEntity, Student.class);
// 		student = response.getBody();
		// ---------------------------------

		println("******** After: " + student.toString());

		assertThat(student.getFirstName()).isEqualTo(firstName);
		assertThat(student.getGroup().getId()).isEqualTo(anotherGroup.getId());
		assertThat(student.getLastName()).isEqualTo(lastName);
		assertThat(student.getBirthDay()).isEqualTo(birthDay);
	}

//	 other integration tests ..
}
