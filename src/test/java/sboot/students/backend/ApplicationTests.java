package sboot.students.backend;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import sboot.students.backend.repos.StudentRepository;

@SpringBootTest
class ApplicationTests {
	
	@Autowired
    StudentRepository studentRepo;

	@Test
	void contextLoads() {
		assertNotNull(studentRepo);
	}
}
