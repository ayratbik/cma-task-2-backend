package sboot.students.backend.repos;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import static sboot.students.backend.AppUtils.*;
import sboot.students.backend.TestDataProvider;
import sboot.students.backend.model.Student;
import sboot.students.backend.model.StudyGroup;

@SpringBootTest()
public class StudentRepositoryIntegrationTest {

	private static final Logger log = LoggerFactory.getLogger(StudentRepositoryIntegrationTest.class);

	@Autowired
	private StudentRepository repository;
	@Autowired
	private TestDataProvider dataProvider;
	@Autowired
	private StudyGroupRepository groupRepository;

	private StudyGroup group;

	@BeforeEach
	void beforeEach() {
		repository.deleteAll();
		group = dataProvider.getTestStudyGroup();
	}

	@Test
	void testRepositoryOperations() {

		println("\n********** START OF testRepositoryOperations() TEST ********\n");
		// repository has no records so far
		assertThat(repository.count()).isEqualTo(0);

		println("******** Prepare new student *******");
		Student student = new Student("Jessy", "Lee");
		TestDataProvider.fillNamedStudentWithTestData(student, group);
		assertThat(student.getId()).isNull();

		println("******** try to persist student");
		try {
			student = repository.save(student);
		} catch (Exception ex) {
			fail("EXCEPTION WAS CAUGHT: " + ex.getClass().getSimpleName() + ": " + ex.getMessage());
		}
		assertThat(repository.count()).isEqualTo(1);
		assertThat(student.getId()).isNotNull();
		log.info("Persisted student's ID is: {}", student.getId());

		println("********** try to find student");
		Optional<Student> found = repository.findById(student.getId());
		if (!found.isPresent()) fail("STUDENT NOT FOUND");
		else println("********** student was found");

		println("********* Another student persist attempt with keyNumber violation");
		Student student2 = new Student("Robert", "Watson");
		TestDataProvider.fillNamedStudentWithTestData(student2, student.getGroup());
		student2.setKeyNumber(student.getKeyNumber());
		try {
			repository.save(student2);
			fail("EXCEPTION WAS EXPECTED: unique constrain violation");
		} catch (DataIntegrityViolationException ex) {
			println("********** OK, Exception was caught!");
			log.info("OK! Exception was caught: {}-{}", ex.getClass().getSimpleName(), ex.getMessage());
		}
		println("\n********** END OF testRepositoryOperations() TEST ********\n");
	}

	@Disabled
	@Test
	public void deleteGroupShouldCauseItsStudentsRemoving(){

		Student student = new Student("Jessy", "Lee");
		TestDataProvider.fillNamedStudentWithTestData(student, group);
		student = repository.save(student);
		groupRepository.delete(group);
		long count = repository.count();
		assertThat(count).isEqualTo(0);

	}
}
