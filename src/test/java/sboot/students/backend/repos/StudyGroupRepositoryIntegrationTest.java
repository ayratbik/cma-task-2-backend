package sboot.students.backend.repos;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static sboot.students.backend.AppUtils.*;
import sboot.students.backend.model.StudyGroup;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

@SpringBootTest()
public class StudyGroupRepositoryIntegrationTest {

	private static final Logger log = LoggerFactory.getLogger(StudyGroupRepositoryIntegrationTest.class);

	@Autowired
	private StudyGroupRepository repository;

	@BeforeEach
	void beforeEach() {
//		repository.deleteAll();
	}

	@Test
	void testRepositoryOperations() {

		println("\n********** START OF testRepositoryOperations() TEST ********\n");
		// repository has no records so far
//		assertThat(repository.count()).isEqualTo(0);

		println("******** Prepare new group *******");
		StudyGroup group = new StudyGroup("abc123");
		assertThat(group.getId()).isNull();

		println("******** try to persist group");
		try {
			group = repository.save(group);
		} catch (Exception ex) {
			fail("EXCEPTION WAS CAUGHT: " + ex.getClass().getSimpleName() + ": " + ex.getMessage());
		}
//		assertThat(repository.count()).isEqualTo(1);
		assertThat(group.getId()).isNotNull();
		log.info("Persisted group's ID is: {}", group.getId());

		println("********** try to find student");
		Optional<StudyGroup> found = repository.findById(group.getId());
		if (!found.isPresent()) fail("STUDENT NOT FOUND");
		else println("********** group was found");

		println("\n********** END OF testRepositoryOperations() TEST ********\n");
	}
}
