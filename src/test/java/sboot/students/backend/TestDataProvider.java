package sboot.students.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sboot.students.backend.model.Student;
import sboot.students.backend.model.StudentsUtils;
import sboot.students.backend.model.StudyGroup;
import sboot.students.backend.repos.StudentRepository;
import sboot.students.backend.repos.StudyGroupRepository;

import java.time.LocalDate;

@Component
public class TestDataProvider {

    @Autowired
    StudentRepository studentRepository;
    @Autowired
    StudyGroupRepository groupRepository;

    private StudyGroup group;

    public static Student createStudentWithTestData(StudyGroup group){
        Student student = new Student("Phil", "Collins");
        return fillNamedStudentWithTestData(student, group);
    }

    public static Student fillNamedStudentWithTestData(Student student, StudyGroup group){
//        student.setFirstName("Phil");
//        student.setLastName("Collins");
        student.setPatronymic("Testovich");
        student.setBirthDay(LocalDate.parse("2004-03-24"));
        student.setKeyNumber(StudentsUtils.getUuidShortKey());
        student.setGroup(group);
        return student;
    }

    public StudyGroup getTestStudyGroup(){
        groupRepository.deleteAll();
        group = groupRepository.save(new StudyGroup("test123"));
        return group;
    }

    public StudyGroup getAnotherTestStudyGroup(){
//        groupRepository.deleteAll();
        group = groupRepository.save(new StudyGroup("321test"));
        return group;
    }

}
